# Changelog for deriveJsonNoPrefix

## 0.1.0.1

- Add tests.
- Add more detailed README.

## 0.1.0.0

Initial release.
